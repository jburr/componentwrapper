"""Component implementations for services"""

from __future__ import annotations

from typing import Any, Optional, Tuple

from componentwrapper.icreatable import is_gaudi
from componentwrapper.component import Component, ComponentArray, ComponentType

if not is_gaudi:
    from AnaAlgorithm.DualUseConfig import createService


class Service(Component):
    @property
    def component_type(self) -> ComponentType:
        return ComponentType.Service

    @property
    def is_public(self) -> bool:
        return True

    def _create_ab_component(
        self,
        sequence: Optional[Any] = None,
        parent: Optional[Any] = None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        return createService(self.type, self.name, sequence=sequence)


class ServiceArray(ComponentArray[Service]):
    @property
    def component_type(self) -> ComponentType:
        return ComponentType.Service

    @property
    def is_public(self) -> bool:
        return True

    def _empty_copy(self) -> ServiceArray:
        return ServiceArray()
