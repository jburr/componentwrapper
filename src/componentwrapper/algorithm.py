"""Wrapper classes for algorithms

Beyond the basic algorithm array class this includes a sequence class that
(in Athena/Gaudi) is an algorithm in its own right
"""

from __future__ import annotations

from typing import Any, Iterable, Mapping, Optional, Tuple
from componentwrapper.icreatable import is_gaudi
from componentwrapper.component import Component, ComponentArray, ComponentType

if is_gaudi:
    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    from AthenaConfiguration.ComponentFactory import CompFactory
else:
    from AnaAlgorithm.DualUseConfig import createAlgorithm


def __flatten(ll: list) -> list:
    flattened = []
    for v in ll:
        if isinstance(v, list):
            flattened += __flatten(v)
        else:
            flattened.append(v)
    return flattened


class Algorithm(Component):
    @property
    def component_type(self) -> ComponentType:
        return ComponentType.Algorithm

    @property
    def is_public(self) -> bool:
        return False

    def _create_ab_component(
        self,
        sequence: Optional[Any] = None,
        parent: Optional[Any] = None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        return createAlgorithm(self.type, self.name)


class AlgorithmArray(ComponentArray[Algorithm]):
    @property
    def component_type(self) -> ComponentType:
        return ComponentType.Algorithm

    @property
    def is_public(self) -> bool:
        return False

    def _empty_copy(self) -> AlgorithmArray:
        return AlgorithmArray()

    def create(
        self,
        *,
        sequence=None,
        accumulator=None,
        parent=None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        created = super().create(
            sequence=sequence, accumulator=accumulator, parent=parent, keys=keys
        )
        # Just for analysis base: flatten the list
        if not is_gaudi:
            return __flatten(created)


class AlgSequence(Algorithm):
    """Algorithm sequence class

    In AnalysisBase this is just a mockup around a list of algorithms and doesn't
    create a true component
    """

    def __init__(
        self,
        name: str,
        Members: Iterable[Algorithm] = (),
        **properties: Mapping[str, Any],
    ):
        super().__init__(type="AthSequencer", name=name, **properties)
        self["Members"] = AlgorithmArray(Members)

    @property
    def children(self) -> AlgorithmArray:
        """The algorithms in the sequence"""
        return self["Members"]

    def create(
        self,
        *,
        sequence=None,
        accumulator=None,
        parent=None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        if not is_gaudi:
            return self.children.create(
                sequence=sequence, accumulator=accumulator, parent=None, keys=()
            )

        new_acc = None
        sequence = CompFactory.getComp(self.type)(self.name)  # type: ignore
        if accumulator is not None:
            new_acc = ComponentAccumulator()
            new_acc.addSequence(sequence)  # type: ignore
        # Now set the properties on this component
        for k, v in self.items():
            if isinstance(v, (Component, ComponentArray)):
                if accumulator is not None or not v.is_public:
                    # Accumulator mode or a private object
                    v = v.create(
                        sequence=sequence,
                        accumulator=new_acc,
                        parent=None,
                        keys=(),
                    )
                else:
                    # Public component: this is created elsewhere and just the type and
                    # name are set here
                    if isinstance(v, Component):
                        v = v.type_and_name
                    else:
                        v = [x.type_and_name for x in v]
            setattr(sequence, k, v)
        if accumulator is not None:
            accumulator.merge(new_acc, sequenceName=sequence)

        return sequence

    def _create_ab_component(
        self,
        sequence: Optional[Any] = None,
        parent: Optional[Any] = None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        assert False
