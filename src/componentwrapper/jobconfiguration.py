"""Class to represent the whole configured job"""

from __future__ import annotations

import collections.abc
import copy
from typing import Any, Dict, Iterable, List, Tuple, overload

from componentwrapper.icreatable import ICreatable
from componentwrapper.component import IComponentSource, Component


class JobConfiguration(
    ICreatable, IComponentSource, collections.abc.MutableSequence[ICreatable]
):
    """Represents the whole configured job"""

    def __init__(self) -> None:
        self._extra_public_components: Dict[str, Component] = {}
        self.__pieces: List[ICreatable] = []

    def add_extra_public_component(self, component: Component):
        """Add an extra public component that must be created"""
        self.merge_public_components(
            self._extra_public_components, {component.name: component}
        )

    @overload
    def __getitem__(self, idx: int) -> ICreatable:
        ...

    @overload
    def __getitem__(self, idx: slice) -> JobConfiguration:
        ...

    def __getitem__(self, idx):
        if isinstance(idx, int):
            return self.__pieces[idx]
        else:
            copy = JobConfiguration()
            copy += self.__pieces[idx]
            return copy

    @overload
    def __setitem__(self, idx: int, value: ICreatable) -> None:
        ...

    @overload
    def __setitem__(self, idx: slice, value: Iterable[ICreatable]) -> None:
        ...

    def __setitem__(
        self, idx: int | slice, value: ICreatable | Iterable[ICreatable]
    ) -> None:
        self.__pieces[idx] = value  # type: ignore

    def __delitem__(self, idx: int | slice) -> None:
        del self.__pieces[idx]

    def __len__(self) -> int:
        return len(self.__pieces)

    def insert(self, idx: int, value: ICreatable) -> None:
        self.__pieces.insert(idx, value)

    def __iadd__(self, other):
        """Append a component to this"""
        if isinstance(other, JobConfiguration):
            for x in other:
                self += x
        elif isinstance(other, ICreatable):
            self.append(other)
        else:
            # Otherwise assume that this is a list of algorithms or fragments
            for x in other:
                self += x
        return self

    def get_public_components(self) -> dict[str, Component]:
        components = copy.deepcopy(self._extra_public_components)
        for child in self:
            if isinstance(child, IComponentSource):
                self.merge_public_components(components, child.get_public_components())
        return components

    def create(
        self,
        *,
        sequence=None,
        accumulator=None,
        parent=None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        # First create public components (if we're not running in CA mode)
        if accumulator is None:
            for component in self.get_public_components().values():
                component.create(
                    sequence=sequence, accumulator=accumulator, parent=parent, keys=keys
                )

        created: List[Any] = []
        for child in self:
            x = child.create(
                sequence=sequence, accumulator=accumulator, parent=parent, keys=keys
            )
            # Lists of algorithms should be placed into the job individually so we
            # manually flatten the list
            if isinstance(x, (list, tuple)):
                created += x
            else:
                created.append(x)
        return created
