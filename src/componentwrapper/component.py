"""Classes to model component types uniformly between Gaudi and AnalysisBase

Jobs in Athena and AnalysisBase are built out of components. Components are written in
C++ but all expose values which can be configured called 'properties'. The classes in
this module model these through dictionary-like access. Components come in three main
types:

Algorithms:
    Algorithms are the primary pieces of the job. They all have an execute method which
    is called once per event (unless the event has been rejected by an upstream
    algorithm).

Tools:
    Tools are more flexible components. Their methods can conform to any interface and
    must (ultimately) be called by algorithms to do their jobs. Tools can be either
    public or private. Public tools are globally available and accessible by name. This
    means that there is only one instance of any named public tool. Generally public
    tools are no longer recommended with a few exceptions (such as the TrigDecisionTool
    which must *always* be public). Private tools are owned by their parent components.

Services:
    Services are global components. They can behave similarly to public tools but are
    generally preferred.
"""

from __future__ import annotations
import abc
import collections.abc
import copy
import enum
import logging
from typing import (
    Any,
    Dict,
    Iterable,
    Iterator,
    Mapping,
    Optional,
    Tuple,
    TypeVar,
    Union,
    cast,
    overload,
)

from componentwrapper.icreatable import ICreatable, is_gaudi

log = logging.getLogger(__name__)

if is_gaudi:
    from AthenaConfiguration.ComponentFactory import CompFactory
    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


class IComponentSource(abc.ABC):
    """Interface for objects that hold public components"""

    @abc.abstractmethod
    def get_public_components(self) -> dict[str, Component]:
        """Return all public components

        Includes the object being queried if it is public

        Raises
        ------
        ValueError
            Multiple versions of the same component cannot be merged
        """

    @staticmethod
    def merge_public_components(
        target: Dict[str, Component], source: Mapping[str, Component]
    ) -> None:
        """Merge two dictionaries containing public components

        Parameters
        ----------
        target : dict[str, Component]
            The target dictionary
        source : Mapping[str, Component]
            The source dictionary

        Raises
        ------
        ValueError
            Two components could not be merged
        """
        for component in source.values():
            try:
                existing = target[component.name]
            except KeyError:
                target[component.name] = copy.deepcopy(component)
            else:
                existing.merge(component)


class ComponentType(enum.Enum):
    """The type of component"""

    Algorithm = enum.auto()
    Tool = enum.auto()
    Service = enum.auto()


class Component(ICreatable, IComponentSource, collections.abc.MutableMapping[str, Any]):
    """Class to represent a single component

    Components act like python dictionaries with the keys and values being the names
    and values of properties. Properties that are components themselves are set through
    the same mechanism and the backend will treat these correctly.

    Arrays of components need to be wrapped in the corresponding ComponentArray class
    """

    def __init__(self, type: str, name: str, **properties: Mapping[str, Any]):
        """Create the component

        Extra kwargs are interpreted as the names and values of properties

        Parameters
        ----------
        type : str
            The concrete component type to create
        name : str
            The name of this component
        """
        self._type = type
        self._name = name
        self.__properties: Dict[str, Any] = {}
        self.update(properties)

    @abc.abstractproperty
    def component_type(self) -> ComponentType:
        """What sort of component this is"""

    @property
    def name(self) -> str:
        """The name of this component"""
        return self._name

    @property
    def type(self) -> str:
        """The concrete C++ type of this component"""
        return self._type

    @property
    def type_and_name(self) -> str:
        """The type and name string"""
        return f"{self.type}/{self.name}"

    @abc.abstractproperty
    def is_public(self) -> bool:
        """Is this a public component (service or public tool)"""

    def get_public_components(self) -> Dict[str, Component]:
        public_components: Dict[str, Component] = {}
        if self.is_public:
            public_components[self.name] = copy.deepcopy(self)
        for v in self.values():
            if isinstance(v, IComponentSource):
                self.merge_public_components(
                    public_components, v.get_public_components()
                )
        return public_components

    def merge(self, other: "Component"):
        """Merge this with another component definition with the same name

        The default implementation will make sure that the type, name and component
        type are the same and then merges the properties. If a property is present in
        both versions it must be equal. Otherwise a set property overrides the default
        value of an unset property.

        This implementation is ignored when using ComponentAccumulators as a backend in
        favour of their merging mechanism.

        Raises
        ------
        ValueError
            The two components are incompatible
        """
        if (
            self.type != other.type
            or self.name != other.name
            or self.component_type != other.component_type
        ):
            log.error(
                f"{self.type_and_name} cannot be merged with {other.type_and_name}"
            )
            raise ValueError
        for k, v in other.items():
            try:
                existing = self[k]
            except KeyError:
                # It doesn't already exist so just add it in
                self[k] = v
            else:
                if isinstance(existing, Component):
                    if not isinstance(v, Component):
                        log.error(f"Property {k} cannot be merged: {existing} != {v}")
                        raise ValueError
                    existing.merge(v)
                elif existing != v:
                    log.error(f"Property {k} cannot be merged: {existing} != {v}")
                    raise ValueError

    def create(
        self,
        *,
        sequence=None,
        accumulator=None,
        parent=None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        new_acc = None
        if is_gaudi:
            backend = CompFactory.getComp(self.type)(self.name)  # type: ignore
            if accumulator is not None:
                # Create everything in a new accumulator and *then* merge that in
                new_acc = ComponentAccumulator()
                # We're in CA mode so add this new public component
                if self.component_type == ComponentType.Service:
                    new_acc.addService(backend)  # type: ignore
                elif self.component_type == ComponentType.Algorithm:
                    new_acc.addEventAlgo(backend)  # type: ignore
                elif len(keys) == 0:
                    # No parent keys => this is public
                    new_acc.addPublicTool(backend)  # type: ignore
        else:
            assert accumulator is None
            backend = self._create_ab_component(
                sequence=sequence, parent=parent, keys=keys
            )
        # Update the parent if necessary
        if not (self.component_type == ComponentType.Tool and not self.is_public):
            parent = backend
        # Now set the properties on this component
        for k, v in self.items():
            if isinstance(v, (Component, ComponentArray)):
                if accumulator is not None or not v.is_public:
                    # Accumulator mode or a private object
                    v = v.create(
                        sequence=sequence,
                        accumulator=new_acc,
                        parent=parent,
                        keys=keys + (k,),
                    )
                else:
                    # Public component: this is created elsewhere and just the type and
                    # name are set here
                    if isinstance(v, Component):
                        v = v.type_and_name
                    else:
                        v = [x.type_and_name for x in v]
            setattr(backend, k, v)
        if accumulator is not None:
            accumulator.merge(new_acc, sequenceName=sequence)
        return backend

    @abc.abstractmethod
    def _create_ab_component(
        self,
        sequence: Optional[Any] = None,
        parent: Optional[Any] = None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        """Create the underlying AnalysisBase component

        Only used in AnalysisBase, not necessary anywhere else

        Parameters
        ----------
        sequence : Any, optional
            The parent AlgSequence object
        parent : Any, optional
            The last algorithm, public tool or service.
        keys : Tuple[str, ...], optional
            The list of keys reaching from 'parent' to this

        Returns
        -------
        Any
            The created AnalysisBase component
        """

    def __getitem__(self, key: str) -> Any:
        return self.__properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.__properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.__properties[key]

    def __iter__(self) -> Iterator[str]:
        return iter(self.__properties)

    def __len__(self) -> int:
        return len(self.__properties)


ComponentT = TypeVar("ComponentT", bound=Component)
SelfT = TypeVar("SelfT", bound="ComponentArray")


class ComponentArray(
    ICreatable, IComponentSource, collections.abc.MutableSequence[ComponentT]
):
    """Base class for arrays of components

    This represents lists of components when set as properties, for example
    PrivateToolArray. The job overall is also an instance of this class as it is a list
    of algorithms.
    """

    def __init__(self, values: Iterable[ComponentT] = ()):
        """Create the array, optionally with a list of initial contents"""
        self.__values: list[ComponentT] = []
        self += values

    @abc.abstractproperty
    def component_type(self) -> ComponentType:
        """The type of components held in this array"""

    @abc.abstractproperty
    def is_public(self) -> bool:
        """Whether the components held within are public"""

    def get_public_components(self) -> Dict[str, Component]:
        """Any public components that are children of any members of this array

        Includes all members of this array if they are public

        Raises
        ------
        ValueError
            Multiple versions of the same component cannot be merged
        """
        public_components: Dict[str, Component] = {}
        for component in self:
            self.merge_public_components(
                public_components, component.get_public_components()
            )
        return public_components

    def create(
        self,
        *,
        sequence=None,
        accumulator=None,
        parent=None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        return [
            v.create(
                sequence=sequence, accumulator=accumulator, parent=parent, keys=keys
            )
            for v in self
        ]

    @abc.abstractmethod
    def _empty_copy(self: SelfT) -> SelfT:
        """Used to create an empty copy of this array"""

    @overload
    def __getitem__(self, idx: int) -> ComponentT:
        ...

    @overload
    def __getitem__(self, idx: slice) -> ComponentArray[ComponentT]:
        ...

    def __getitem__(
        self, idx: Union[int, slice]
    ) -> Union[ComponentT, ComponentArray[ComponentT]]:
        if isinstance(idx, int):
            return self.__values[idx]
        else:
            copy = self._empty_copy()
            copy += self.__values[idx]
            return copy

    @overload
    def __setitem__(self, idx: int, value: ComponentT):
        ...

    @overload
    def __setitem__(self, idx: slice, value: Iterable[ComponentT]):
        ...

    def __setitem__(
        self, idx: Union[int, slice], value: Union[ComponentT, Iterable[ComponentT]]
    ):
        if isinstance(idx, int):
            value = cast(ComponentT, value)
            if value.component_type != self.component_type:
                raise ValueError(
                    f"Cannot add a {value.component_type} into an array of "
                    + "{self.component_type}s"
                )
            self.__values[idx] = value
        else:
            value = cast(Iterable[ComponentT], value)
            for v in value:
                if v.component_type != self.component_type:
                    raise ValueError(
                        f"Cannot add a {v.component_type} into an array of "
                        + "{self.component_type}s"
                    )
            self.__values[idx] = value

    def __delitem__(self, idx: Union[int, slice]):
        del self.__values[idx]

    def __iter__(self) -> Iterator[ComponentT]:
        return iter(self.__values)

    def insert(self, idx: int, value: ComponentT) -> None:
        if value.component_type != self.component_type:
            raise ValueError(
                f"Cannot add a {value.component_type} into an array of "
                + "{self.component_type}s"
            )
        self.__values.insert(idx, value)

    def __len__(self) -> int:
        return len(self.__values)
