"""Interface class for objects that create components in one of the back-ends"""

import abc
from typing import Any, Tuple

try:
    import Gaudi  # noqa:F401

    is_gaudi = True
except ImportError:
    is_gaudi = False


class ICreatable(abc.ABC):
    """Interface class for all pieces of a configured job"""

    @abc.abstractmethod
    def create(
        self,
        *,
        sequence=None,
        accumulator=None,
        parent=None,
        keys: Tuple[str, ...] = ()
    ) -> Any:
        """Create the component in the relevant backend

        The combination of parameters depends on the backend that will be used.

        sequence:
            Must be an AlgSequence for AnalysisBase, can be a string for
            ComponentAccumulators
        accumulator:
            The parent ComponentAccumulator in CA mode and None in all others
        parent:
            The last algorithm, public tool or service. Used for creating private tools.
            Ignored in CA mode
        keys:
            The list of keys reaching from 'parent' to this. Ignored in CA mode

        Returns
        -------
        The created components. For CA mode this will be the relevant gaudi
        configurable. Public tools, algorithms and services will have been merged into
        'accumulator' but private tools will be unmerged.
        """
