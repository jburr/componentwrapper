"""Wrappers for ATLAS component configurations

Ensures that they can be handled identically for the different backend systems:
- AnalysisBase
- Athena (legacy)
- Athena ComponentAccumulators
"""

__version__ = "1.0.0"
