"""Wrappers for preconfigured algorithms/ComponentAccumulators"""

from typing import Any, Tuple
from componentwrapper.icreatable import ICreatable


class PreconfiguredAlgList(ICreatable):
    """Preconfigured list of algorithms"""

    def __init__(self, algorithms):
        """Create the fragment

        Parameters
        ----------
        algorithms
            The already configured algorithms
        """
        self._algorithms = algorithms

    def create(
        self,
        *,
        sequence=None,
        accumulator=None,
        parent=None,
        keys: Tuple[str, ...] = ()
    ) -> Any:
        if accumulator is not None:
            raise ValueError("Cannot convert preconfigured algorithms to CA format")
        else:
            return self._algorithms
