"""Wrappers for tools"""

from __future__ import annotations

from typing import Any, Iterable, Mapping, Optional, Tuple

from componentwrapper.component import Component, ComponentArray, ComponentType
from componentwrapper.icreatable import is_gaudi

if not is_gaudi:
    from AnaAlgorithm.DualUseConfig import (
        addPrivateTool,
        addPrivateToolInArray,
        createPublicTool,
    )


class Tool(Component):
    """Wrapper for tools"""

    def __init__(
        self,
        type: str,
        name: str,
        public: bool = False,
        in_array: bool = False,
        **properties: Mapping[str, Any],
    ):
        """Create the tool

        Parameters
        ----------
        type : str
            The concrete C++ type
        name : str
            The name of the tool
        public : bool, optional
            If this is a public tool, by default False
        in_array : bool, optional
            If this tool is in an array, by default False
        """
        super().__init__(type, name, **properties)
        self.__public = public
        self.__in_array = in_array

    @property
    def component_type(self) -> ComponentType:
        return ComponentType.Tool

    @property
    def is_public(self) -> bool:
        return self.__public

    @is_public.setter
    def is_public(self, value: bool) -> None:
        self.__public = value

    @property
    def in_array(self) -> bool:
        """Is this tool in an array

        This is only needed to correctly create the tool in AnalysisBase
        """
        return self.__in_array

    @in_array.setter
    def in_array(self, value: bool) -> None:
        self.__in_array = value

    def _create_ab_component(
        self,
        sequence: Optional[Any] = None,
        parent: Optional[Any] = None,
        keys: Tuple[str, ...] = (),
    ) -> Any:
        if self.is_public:
            component = createPublicTool(self.type, self.name)
            # For some reason the createPublicTool method doesn't do this, even
            # though the createService method does...
            sequence += component
        else:
            assert parent
            assert keys
            if self.in_array:
                addPrivateToolInArray(parent, ".".join(keys), self.type)
            else:
                addPrivateTool(parent, ".".join(keys), self.type)
            component = parent
            for key in keys:
                component = getattr(component, key)
            if self.in_array:
                # The correct component is the one that we just added.
                component = component[-1]
        return component


class ToolArray(ComponentArray[Tool]):
    """Wrapper for arrays of tools"""

    def __init__(self, values: Iterable[Tool] = (), public: bool = False):
        """Create the array

        Parameters
        ----------
        values : Iterable[Tool], optional
            Tools to include in the array, by default ()
        public : bool, optional
            Whether the tools in the array are public, by default False
        """
        self.__public = public
        super().__init__(values)

    @property
    def component_type(self) -> ComponentType:
        return ComponentType.Tool

    @property
    def is_public(self) -> bool:
        return self.__public

    @is_public.setter
    def is_public(self, value: bool) -> None:
        self.__public = value
        for tool in self:
            tool.is_public = value

    def _empty_copy(self) -> ToolArray:
        return ToolArray()

    def __setitem__(self, idx: int | slice, value: Tool | Iterable[Tool]):
        super().__setitem__(idx, value)  # type: ignore
        if isinstance(value, Tool):
            value.in_array = True
            value.is_public = self.is_public
        else:
            for t in value:
                t.in_array = True
                t.is_public = self.is_public

    def __delitem__(self, idx: int | slice):
        if isinstance(idx, int):
            self[idx].in_array = False
        else:
            for t in self[idx]:
                t.in_array = False
        return super().__delitem__(idx)

    def insert(self, idx: int, value: Tool) -> None:
        super().insert(idx, value)
        value.in_array = True
        value.is_public = self.is_public
